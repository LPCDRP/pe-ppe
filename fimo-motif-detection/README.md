# Scripts and data files associated with FIMO detection analysis of pe/ppe genes

### Dependencies
* python2.7
* R 3.3.3
* [MEME Suite 5.0.5](http://meme-suite.org/doc/download.html "MEME suite homepage")

### Usages
Runs fimo motif detection on given fasta  
default output directory is "fimo_out"  
`Datafile1-PE-pwm.txt` and `Datafile2-PPE-pwm.txt` located in `data` subdirectory
```
./run_fimo [-h] [-m] [-f] [-t]

Where:
	-h show this help menu
	-m MEME motif file (Datafile1-PE-pwm.txt or Datafile2-PPE-pwm.txt)
	-f FASTA file
	-t threshold (2.14e-16 for PE 1.21e-33 for PPE)
```
Prints PE and PPE threshold dataframe with false negative rate, false positive rate, sensitivity and specificity  
requires files "Rv-PE-fimo.tsv", "Rv-PPE-fimo.tsv", "PEnames.txt", "PPEnames.txt", "rvandnames.tsv" to run
```
./FIMO-Rv-threshold.R  
```

Accepts a multisequence FASTA of PE or PPE and a family name and returns a list of genes for each subfamily based on motifs described in Gey van Pittius et al. 2006.
```
./subfamily-motif-discovery.py [-h] [-s FASTA] [-f FAMILY]

optional arguments:
  -h, --help            show this help message and exit
  -s FASTA, --fasta FASTA
                        Multisequence amino acid FASTA with gene name as the
                        ID
  -f FAMILY, --family FAMILY
                        Family name, choices are PE or PPE)
```
