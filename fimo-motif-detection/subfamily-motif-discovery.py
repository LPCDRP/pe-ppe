#!/usr/bin/env python2.7

import re
import argparse
from Bio import SeqIO


def ppe_svp(s):
    hit = re.search('G[A-Z]{2}SVP[A-Z]{2}W', str(s[199:]))
    if hit:
        return True
    return False


def ppe_ppw(s):
    hit = re.search('P[A-Z]{2}P[A-Z]{2}W', str(s[199:]))
    hit2 = re.search('GF[A-Z]GT', str(s[199:]))
    if hit and hit2:
        return True
    return False


def ppe_mptr(s):
    # Returns a list of each hit
    hits = re.findall('N[A-Z]G[A-Z]GN[A-Z]G', str(s[199:]))
    if hits:
        return [True, len(hits)]
    return [False]


def pe_pgrs(s):
    # Returns a list of each hit
    hits = re.findall('GG[AN]', str(s[109:]))
    if len(hits) >= 2:
        return [True, len(hits)]
    return [False]


def id_motif(fasta, family):

    if family.lower() == 'ppe':
        svp = []
        ppw = []
        mptr = []
        non_motif = []
        for record in SeqIO.parse(fasta, 'fasta'):
            if ppe_svp(record.seq) is True:
                svp.append(record.id)
            elif ppe_ppw(record.seq) is True:
                ppw.append(record.id)
            elif ppe_mptr(record.seq)[0] is True:
                copy_num = ppe_mptr(record.seq)[1]
                mptr.append(record.id)
            else:
                non_motif.append(record.id)
        return {'svp': svp, 'ppw': ppw, 'mptr': mptr, 'ppe-non-motif': non_motif}
    elif family.lower() == 'pe':
        pgrs = []
        non_motif = []
        for record in SeqIO.parse(fasta, 'fasta'):
            if pe_pgrs(record.seq)[0] is True:
                copy_num = pe_pgrs(record.seq)[1]
                pgrs.append(record.id)
            else:
                non_motif.append(record.id)
        return {'pgrs': pgrs, 'pe-non-motif': non_motif}


def arguments():
    parser = argparse.ArgumentParser(description='Accepts a multisequence FASTA of PE or PPE and'
                                                 ' a family name and returns a list of genes for '
                                                 'each subfamily based on motifs described in Gey van '
                                                 'Pittius et al. 2006.')
    parser.add_argument('-s', '--fasta', help='Multisequence amino acid FASTA with gene name as the ID')
    parser.add_argument('-f', '--family', help='Family name, choices are PE or PPE)')
    args = parser.parse_args()
    return args


def main():
    command_line_args = arguments()
    gene_lists = id_motif(command_line_args.fasta, command_line_args.family)
    for filename, l in gene_lists.iteritems():
        with open(filename, 'w') as output:
            for gene in l:
                output.write('\t'.join([gene, filename]) + '\n')


if __name__ == '__main__':
    main()
