#!/usr/bin/env Rscript

library(ggplot2)
library(data.table)
options(scipen = 100000)

args= commandArgs(trailingOnly = T)

# test if there is at least 2 arguments: if not, return an error
if (length(args)<2) {
  stop("At least two arguments must be supplied \n 
       (input files, one for MHC class i epitope predicitions, one for MHC class ii).\n
       Usage: Rscript tcellepitopes-graph.R [file1(MHC class i)] [file2(MHC class ii)]\n
       default output files are class-i-tcell.png and class-ii-tcell.png", call.=FALSE)
} 

#creating bins, if max amino acid length is greater than 4500, increase here
bins=seq(0,4500,by=50)
labels <- gsub("(?<!^)(\\d{3})$", ",\\1", bins, perl=T)
rangelabels=paste(head(labels,-1), tail(labels,-1), sep="-")

## PGRS pan class 1
pgrs=fread(args[1],header=T)
threshpgrs=subset(pgrs,pgrs$ic50<500)
threshpgrs$affinity[threshpgrs$ic50>50]="intermediate affinity"
threshpgrs$affinity[threshpgrs$ic50<=50]="high affinity"
threshpgrs$subfamily="PGRS"

threshpgrs$Bin=cut(threshpgrs$start, bins, rangelabels)

shapecolors=c("#56B4E9","#E69F00")

ggplot(threshpgrs, aes (x=Bin, group=affinity)) + 
  theme_bw()+
  geom_point(stat='count', size=3, aes(shape=affinity, color=affinity)) + 
  geom_line(stat='count')+
  geom_vline(aes(xintercept=3), linetype="dashed")+
  scale_color_manual(values=shapecolors)+
  xlab("Amino acid position")+
  ylab("Number of Predicted MHC Class I T Cell Epitopes")+
  theme(legend.position = "top",legend.title = element_blank(),legend.text=element_text(size=12), 
        axis.title=element_text(size=13),
        axis.text.x = element_text(color="black",size=12, angle = -50, hjust = 0),
        axis.text.y=element_text(color="black", size=12),
        plot.margin = margin(r=35,l=12, unit="pt"))+
  ggsave("class-i-tcell.png", width = 11, height = 7.64, units = "in",dpi=300)


### PGRS pan class 2
pgrsclass2=fread(args[2], header=T)
threshpgrs2=subset(pgrsclass2,pgrsclass2$ic50<500)
threshpgrs2$affinity[threshpgrs2$ic50>50]="intermediate affinity"
threshpgrs2$affinity[threshpgrs2$ic50<=50]="high affinity"
threshpgrs2$subfamily="PGRS"

threshpgrs2$Bin=cut(threshpgrs2$start, bins, rangelabels)
class2colors=c("#009E73","#F0E442")

ggplot(threshpgrs2, aes (x=Bin, group=affinity)) + 
  theme_bw()+
  geom_point(stat='count', size=3, aes(shape=affinity, color=affinity)) + 
  geom_line(stat='count')+
  geom_vline(aes(xintercept=3), linetype="dashed")+
  scale_color_manual(values=shapecolors)+
  xlab("Amino acid position")+
  ylab("Number of Predicted MHC Class II T Cell Epitopes")+
  theme(legend.position = "top",legend.title = element_blank(),legend.text=element_text(size=12), 
        axis.title=element_text(size=13),
        axis.text.x = element_text(color="black",size=12, angle = -50, hjust = 0),
        axis.text.y=element_text(color="black", size=12),
        plot.margin = margin(r=35,l=12, unit="pt"))+
  ggsave("class-ii-tcell.png", width = 11, height = 7.64, units = "in", dpi=300)
