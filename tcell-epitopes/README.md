# Scripts associated with pe/ppe analysis 

### Dependencies
* python2.7
* R 3.3.3
1.  ggplot2
2.  data.table
* [IEDB MHC-I binding predictions tool](http://tools.iedb.org/mhci/download/ "IEDB MHC-I")
* [IEDB MHC-II binding predictions tool](http://tools.iedb.org/mhcii/download/ "IEDB MHC-II")

### Usages
It is recommended to add downloaded IEDB tools directory to PATH
```
export PATH=$PATH:/path/to/mhc_i/src
export PATH=$PATH:/path/to/mhc_ii
```
Runs predict_binding.py from IEDB MHC-I tools with selected epitopes
```
./run_predictMHCI [-h] [-f]  
Where:
	-h show this help menu
	-f FASTA file
	default prints to standard out
```
Runs mhc_II_binding.py from IEDB MHC-II tools.  
This step can be computationally intensive for FASTA files with many sequences.
```
./run_predictMHCII [-h] [-f]`   
Where:
	-h show this help menu
	-f FASTA file
	default prints to standard out
```

Creates T cell epitope prediction graphs that plots the number predicted epitope per amino acid location
```
./tcellepitopes-graph.R [file1(MHC class i)] [file2(MHC class ii)]
    input files: MHC class i epitope predicitions & MHC class ii epitope predictions
    default output files are class-i-tcell.png and class-ii-tcell.png
```