# Datafiles associated with pe/ppe analysis 

### Usages
Datafile1-PE-pwm.txt and Datafile2-PPE-pwm.txt are the motif inputs for `run_fimo` located in the `fimo-motif-detection` directory

human-mhcII-epitopes.txt contains all MHCII epitopes used for prediction in `run_predictMHCII`  in the `tcell-epitopes` directory


[1]